CONTENTS OF THIS FILE
---------------------
   
 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

Domain Variable aims to be a replacement variable handling module for Domain
Access, allowing mixed variable realms (domain x language) to be configured.

In other words, it allows setting different variable values for each domain and
for each language at the same time.

Domain Variable supports almost all of the functionality provided by Domain
Configuration, Domain Settings and Domain Theme. Differences are listed in the
documentation.


REQUIREMENTS
------------

Domain Variable requires the following modules:

 * Domain Access (https://drupal.org/project/views)
 * Variable (https://drupal.org/project/variable)
 * Variable store (https://drupal.org/project/variable)
 * Variable realm (https://drupal.org/project/variable)


INSTALLATION
------------

Install as you would normally install a contributed Drupal module. See:
https://drupal.org/documentation/install/modules-themes/modules-7 for further
information.


CONFIGURATION
-------------

 * Configure your Domain Variables in Structure » Domains » Variables
 

MAINTAINERS
-----------

Current maintainers:
 * Benedikt Forchhammer (bforchhammer) - https://www.drupal.org/user/216396
 * Renato Gonçalves (RenatoG) - https://www.drupal.org/user/3326031
 * Jose Antonio R. del Prado (Jose Reyero) - https://www.drupal.org/user/4299
 * Ken Rickard (agentrickard) - https://www.drupal.org/user/20975
 * Niels de Feyter (ndf) - https://www.drupal.org/user/599438
